\input{preamble}
\usepackage{tikzit}
\input{figures/styles.tikzstyles}

\title{Chapitre V : Gaz réels, transitions de phases}

\begin{document}
	%\includecourse

	\section{Gaz réels}%
	\label{sec:gaz_reels}
	
	\subsection{Définition}%
	\label{sub:definition}
	
	\begin{defi}
		Contrairement à un gaz parfait, les molécules d'un gaz réel :
		\begin{enumerate}
			\item ont une taille non négligeable
			\item interagissent entre elles : les molécules éloignées s'attirent, les
				molécules proches se repoussent
			\item les molécules peuvent se condenser et former une phase liquide ou solide
		\end{enumerate}
		Par analogie à un gaz parfait, les molécules d'un gaz réel, d'un liquide ou d'un
		solide sont soumises à une agitation thermique.
	\end{defi}

	\subsection{Équation de Van der Waals}%
	\label{sub:equation_de_van_der_waals}
	
	Termes correctifs à l'équation des gaz parfaits :
	\begin{itemize}
		\item Pour les molécules suffisamment éloignées, on mesure les forces
		d'attraction. 

		Cela crée une pression supplémentaire. On remplace $P$ par
		$P+\frac{E_{pot}}{V}= P+a \left( \frac{\nu}{V}\right)^2$

		\item Pour les molécules suffisamment proches, on mesure les forces de répulsion

		On ne peut pas comprimer le gaz au dessous d'un volume minimal $V_{min}=\nu b$

		On remplace $V$ par $V- \nu b$
	\end{itemize}
	\begin{fml}
		\textbf{Équation de Van der Waals} :
		\begin{align*}
			\left( P+a\left( \frac{\nu}{V} \right)^2 \right)\left( V-\nu b \right) = \nu RT
		\end{align*}
		$a,b$ : constantes qui dépendent du gaz
	\end{fml}

	\subsection{Isothermes du gaz réel}%

	\ctikzfig{c5-1-1}

	$T_C$ est la température critique, disparition de différence entre gaz et liquide
	
	
	% \ctikzfig{c5-1-2} Incomplet

	Figure 2. Zones :
	\begin{itemize}
		\item I : liquide
		\item II : vapeur sèche (phase gazeuse)
		\item III : liquide + vapeur
		\item IV : liquide hyper critique
	\end{itemize}
	\begin{itemize}
		\item DC : courbe d'ébullition
		\item CE : courbe de rosée
		\item point C : point critique, où disparait différent entre liquide et gaz
	\end{itemize}

	Compression isotherme $FBAG$ à $T_C$ 

	$FB$ : compression de vapeur sèche jusqu'au début de condensation au point $B$.

	$BA$ : palier de condensation, la quantité de liquide augmente jusqu'à la condensation
	complète au point $A$ 

	$AG$ : compression du liquide par compression isotherme $HI$ à $T>T_C$ 

	On ne pourra jamais condenser un fluide hyper critique en le comprimant à $T$
	constant. 

	Exemple : air, $T_C=\SI{130}{\celsius}$, air fluide hyper critique à $T$
	ambiante

	\section{Equations différentielles de la thermodynamique}%
	Ce sont des relations entre les différentielles de l'énergie interne, de l'entropie et
	de l'enthalpie et des variables d'état $V, P, T$ des gaz réels, des liquides ou des
	solides

	\subsection{Énergie interne $U( T,V )$. Entropie $S( T,V )$}%
	
	\begin{enumerate}
		\item On choisit un couple de variables indépendantes $T$ et $V$.

		$U=f_1( T,V,P( T,V ) )=f_2( T,V )$ 

		Différentielle totale de $U$ :
		\begin{align}
			dU = \left( \frac{\partial U}{\partial T} \right)_{V} dT + \left( \frac{\partial
			U}{\partial V} \right)_T dV
		\end{align}

		\item $\left( \frac{\partial U}{\partial T} \right)_V$
		
		1er principe pour transformation infinitésimale :
		\begin{align}
			dU = \delta W + \delta Q = -PdV + \delta Q
		\end{align}

		A $V=const, dV=0$ :
		\begin{align*}
			( 2 )\implies&dV = \delta Q_V = \nu c_v dT \\
			\implies & \left( \frac{\partial U}{\partial T} \right)_V = \nu c_v
			\numberthis
		\end{align*}

		\item $\left( \frac{\partial U}{\partial V} \right)_T$ 

		1er principe pour transfo infinitésimale réversible : $\delta Q = TdS$ 
		\begin{align*}
			( 2 ) \implies& dU = -PdV+TdS \numberthis
		\end{align*}
		\begin{align*}
			\left.
			\begin{NiceMatrix}
				\left( \frac{\partial U}{\partial V} \right)_T \\
				\left( \frac{\partial S}{\partial V} \right)_T = \left( \frac{\partial
				P}{\partial T} \right)_V
			\end{NiceMatrix}
			\right\} \implies \left( \frac{\partial U}{\partial V} \right)_T = T\left(
			\frac{\partial U}{\partial T} \right)_V -P \numberthis
		\end{align*}

		\item 
		\begin{fml}
			\begin{align*}
				( 3 ),( 5 ) \to ( 1 ) \implies& dU = \nu c_v dT + T\left( \frac{\partial
				U}{\partial T} \right)_V - P \numberthis
			\end{align*}
		\end{fml}
		$\nu c_v dT$ correspond à la variation de l'énergie cinétique des molécules avec
		variation de $T$ 

		Le deuxième terme correspond à la variation de l'énergie potentielle d'interaction
		entre molécules avec variation de $V$

		\item Entropie $S( T,V )$ 
		\begin{align*}
			( 4 )\implies dS &= \frac{1}{T} dU + \frac{P}{T} dV\\
			&= \nu c_v \frac{dT}{T}+ \left( \frac{\partial P}{\partial T} \right)_V dV -
			\frac{P}{T}dV + \frac{P}{T}dV\\
			&= \nu c_v \frac{dT}{T} + \left( \frac{\partial P}{\partial T} \right)_V dV
			\numberthis
		\end{align*}
	\end{enumerate}
	
\end{document}
