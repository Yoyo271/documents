\input{preamble}


\title{Chapitre II : Théorie cinétique des gaz parfaits}

\begin{document}
	\maketitle

	\section{Modèle d'un gaz parfait}%
	\label{sec:modele_d_un_gaz_parfait}


	\begin{itemize}
		\item Les molécules sont assimilées à des sphères dures de diamètre
			négligeable SCH6
		\item Leur diamètre est très inférieure à la distance qui les sépare
		\item Les seules interactions entre molécules sont des collisions élastiques
		\item Les molécules ne s'attirent pas ni ne se repoussent
	\end{itemize}

	Les molécules de gaz se déplacent à grande vitesse (500 m/s). Elles subissent des
	collisions entre elles ($10^8$ par seconde). La trajectoire des molécules devient très
	désordonnée : séquence de droites entre les différentes collisions.

	Caractéristiques :
	\begin{enumerate}
		\item Le gaz occupe tout le volume offert
		\item Isotropie de vitesse

		La vitesse moyenne des molécules se déplaçant suivant une direction donnée ne
		dépend pas de cette direction 
	\end{enumerate}

	\section{Pression d'un gaz parfait}%
	\label{sec:pression_d_un_gaz_parfait}
	
	On cherche une expression $P=f( n = \frac{N}{V}, m, u )$ avec : $n$ : concentration, $m$ : masse, $u$ : vitesse

	\subsection{Hypothèses}%
	\label{sub:hypotheses}
	
	\begin{itemize}
		\item toutes les molécules ont la même vitesse $u$ 
		\item les molécules se déplacent selon x $\pm$, y $\pm$, z  $\pm$
		 \item $\frac{1}{6}$ des molécules se déplace dans chaque direction
	\end{itemize}

	\subsection{Enchainement de calcul}%
	\label{sub:enchainement_de_calcul}
	
	SCH7

	On utilise la deuxième loi de Newton :
	\begin{align*}
		&F_{\text{gaz -> paroi}} = \frac{\Delta	\vec{p}}{\Delta t} \\
		\iff& F_{\text{gaz -> paroi}} \cdot \Delta t = \Delta \vec{p}\\
		\iff& P \cdot \Delta S \cdot \Delta t = \underbrace{N_{\text{coll}}}_{\text{n° de
		chocs pendant $\Delta t$}} \cdot   \underbrace{\Delta p_1}_{\text{variation quantité de
mouvement d'une molécule}}
	\end{align*}

	SCH8

	Parmi 6 molécules se trouvant dans le volume $V=\Delta S u \Delta T$, une seule
	molécule subit un choc avec la paroi

	Parmi $N$ molécules, on a donc $N_{\text{coll}}=\frac{1}{6}N$ 

	Par définition, la concentration $n=\frac{N}{V}\implies N=nV$ 

	D'où : 
	\begin{fml}
		\begin{align*}
			N_{\text{coll}}=\frac{1}{6}n \cdot \Delta S \cdot u \cdot \Delta t
		\end{align*}
	\end{fml}

	SCH9

	Choc élastique : $\vec{ u_r }=-\vec{ u_i }$ et $\abs{ \vec{ u_r } }=\abs{ \vec{ u_i }
	}=u$ 

	Variation de quantité de mouvement de la molécule :
	\begin{align*}
		\Delta \vec{ p_1 }=m \vec{ u_r }-m\vec{ u_i }=-2m\vec{ u_i }
	\end{align*}
	En valeur absolue :
	\begin{align*}
		\Delta p_1 = \abs{ \vec{ \Delta p_1 } }=\abs{ -2 m u_i }= 2m u
	\end{align*}

	D'où :
	\begin{align*}
		P \Delta S \Delta t = \frac{1}{6} n \Delta S u \Delta t 2m u\\
	\end{align*}
	Finalement :
	\begin{fml}
		\begin{align}
			P = \frac{1}{3}n m u^2
		\end{align}
	\end{fml}
	C'est \textbf{l'équation principale de la cinétique des gaz parfaits} 

	Elle reste valable pour un gaz dont les molécules se déplacent dans tous les sens
	possibles avec des vitesses différentes.

	$u$ est la vitesse quadratique moyenne : 
	\begin{align*}
		u^2 = \frac{\sum_{k=1}^{N} v_i^2}{N} 
	\end{align*}

	\section{Énergie d'un gaz parfait monoatomique}%
	\label{sec:energie_d_un_gaz_parfait_monoatomique}
	
	\setcounter{equation}{0}

	\subsection{Relation entre $U$ et $P$}%
	\label{sub:relation_entre_u_et_p_}

	Par définition de l'énergie interne :
	\begin{align}
		U&= \sum_{k=1}^{N} ( Ecin + Epot ) \nonumber \\
		\shortintertext{L'énergie potentielle est nulle, car pas d'interactions entre molécules d'un gaz
	parfait}
		&= N \frac{1}{2} m u^2
	\end{align}

	$\frac{1}{2}m u^2$ représente l'énergie cinétique moyenne des molécules

	Pression : 
	\begin{align}
		P&=\frac{1}{3}nmu^2 \nonumber\\
		 &=\frac{2}{3}\frac{N}{V} \frac{1}{2}mu^2 \nonumber\\
		 &= \frac{2}{3} \frac{U}{V}
	\end{align}
	D'où :
	\begin{align}
		U=\frac{3}{2}PV
	\end{align}

	\subsection{Relation entre $U$ et $T$}%
	\label{sub:relation_entre_u_et_t_}
	
	On utilise l'équation d'état des gaz parfaits : $PV=\nu RT$ 
	\begin{align*}
		PV&= \nu RT \\
		&= N \underbrace{\frac{R}{N_A}}_{k \text{ cte universelle de Boltzman}} RT
	\end{align*}
	D'où :
	\begin{equation}
		PV=NkT
	\end{equation}
	avec $k = \frac{R}{N_A}= \SI{1.38e-23}{\joule\per\kelvin}$ 

	On déduit de $( 1 ), ( 3 )$ :
	\begin{equation}
		U=\frac{3}{2}NkT
	\end{equation}
	En même temps :
	\begin{equation} \tag{1}
		U=N  E = N( \frac{1}{2} mu^2 )
	\end{equation}
	D'où :
	\begin{fml}
		\begin{align}
			E = \frac{1}{2}mu^2=\frac{3}{2}kT
		\end{align}
	\end{fml}

	La température absolue $T$ d'un gaz parfait est une mesure de l'énergie cinétique
	moyenne de ses molécules.

	Dans le cas général, la température d'une matière (gaz, liquide, solide) traduit le
	degré d'agitation des particules qui la constitue.
	
	\begin{rmq}
		gaz monoatomique : $E=\frac{3}{2}kT$ 

		gaz polyatomique : $E = \alpha kT, \alpha > \frac{3}{2}$ 
	\end{rmq}
	\begin{rmq}
		Forme alternative de l'équation d'état des gaz parfaits :
		\begin{equation}
			( 4 ) \implies PV = NkT \implies P = nkT
		\end{equation}
	\end{rmq}

	\subsection{Application : refroidissement d'un gaz par laser}%
	\label{sub:application_refroidissement_d_un_gaz_par_laser}
	
	\begin{figure}[htpb]
		\centering
		\incfig{refroid_laser}
	\end{figure}

	Pour ralentir les atomes se déplaçant dans un sens quelconque, on installe six
	faisceaux laser suivant les trois directions orthogonales. Suite aux absorptions
	consécutives, les atomes de gaz ralentissent jusqu'à une vitesse de l'ordre de
	$\SI{10}{\milli\meter\per\second}$

	\section{Gaz dans le champ de pesanteur}%
	\label{sec:gaz_dans_le_champ_de_pesanteur}

	\setcounter{equation}{0}

	Hypothèses :
	\begin{itemize}
		\item air est un gaz parfait
		\item $g$ est une constante
		\item $T$ est une constante
	\end{itemize}

	\subsection{}%

	SCH10

	SCH11

	\begin{align}
		dP \cdot S = -dMg
	\end{align}

	\subsection{Masse de la couche d'air}%
	\begin{align}
		dM=m \cdot dN
	\end{align}
	Avec $m$ masse d'une molécule, $dN$ le nombre de molécules dans la couche

	Par définition de la concentration :
	\begin{align}
		n = \frac{dN}{dV} \implies dN = n\cdot dV= nSdt
	\end{align}

	On déduit de $( 3 ),( 2 ),( 1 )$ :
	\begin{align}
		dP \cdot S=-m n S g dz
	\end{align}

	\subsection{Equation d'état des gaz parfaits}%
	
	\begin{align}
		P&=nkT \\
		dP &= d( n\underbrace{kT}_{\text{const}} ) = kT dn
	\end{align}

	On déduit de $( 6 ),( 4 )$ :
	\begin{align*}
		kT dn = -mng dz \iff& \frac{dn}{n} = - \frac{Mg}{kT}dz \\
		\iff& \int \frac{dn}{n} = - \int \frac{mg}{kT} dz\\
		\iff& \ln(n) = -\frac{mgz}{kT} + \underbrace{C_1}_{=\ln(C_2)}\\
		\iff& \ln\left(\frac{n}{C_2} \right) = \frac{-mgz}{kT} \\
		\iff& n = C_2 \cdot \exp(- \frac{mgz}{kT}) \numberthis 
	\end{align*}

	\subsection{Condition initiale : concentration donnée $n_0$ à la surface de la Terre}%
	
	On a $n( 0 )=n_0$. On remplace dans $( 7 )$ :
	\begin{align*}
		n( 0 )=n_0=C_2 \cdot \exp\left( \frac{-mg \times 0}{kT} \right)
	\end{align*}
	On en déduit que $C_2=n_0$ et :
	\begin{align}
		n( z )=n_0 \exp\left(\frac{-mgz}{kT}\right)
	\end{align}
	
	\subsection{Pression atmosphérique}%
	
	On déduit de $( 8 ),( 5 )$ :
	\begin{align*}
		P( z )=n_0 kT \cdot \exp\left(-\frac{mgz}{kT}\right)
	\end{align*}

	\Finalement : Formule barométrique :
	\begin{fml}
		\begin{align}
			P( z )&=P_0 \cdot \exp\left(- \frac{mgz}{kT}\right)\\
				  &= P_0 \cdot \exp(- \frac{\mu gz}{kT})
		\end{align}
	\end{fml}
	\subsection{Application}%
	
	Altimètre barométrique : on mesure la pression et on en déduit l'altitude.

	Remarque : la variation de pression atmosphérique est négligeable à l'échelle de
	quelques mètres.


	\section{Loi de distribution de Boltzmann}%
	\label{sec:loi_de_distribution_de_boltzmann}
	
	\setcounter{equation}{0}

	Le nombre de molécules d'air se trouvant à l'altitude $z$ et donc ayant l'énergie
	potentielle $\epsilon_{pot}= mgz$ est proportionnel : 
	\begin{align*}
		n \propto \exp(- \frac{\epsilon_{pot}}{kT})
	\end{align*}
	Le facteur est appelé facteur de Boltzmann.
	
	Boltzmann a étendu cette conclusion à plusieurs systèmes microscopiques.

	\subsection{Distribution discrète}%
	\label{sub:distribution_discrete}
	
	Suite à l'agitation thermique, l'énergie d'une particule microscopique n'est pas fixe
	mais prend l'une des valeurs $\epsilon_i$ du spectre $( \epsilon_1, \ldots, \epsilon_n )$ 

	Il est impossible de savoir exactement l'énergie d'une molécule à un instant donné
	mais on peut deviner de sa valeur $\epsilon_i$ une certaine probabilité.

	La probabilité que l'énergie d'une particule donnée prenne une valeur $\epsilon_i$ du
	spectre discontinu est proportionnelle au facteur de Boltzmann.

	Énoncé :
	\begin{align}
		\Pi_i = C \cdot \exp\left(- \frac{\epsilon_i}{kT}\right)
	\end{align}

	Cette même probabilité est égale au nombre relatif 
	\begin{align*}
		\frac{N_i}{N_{tot}} \%
	\end{align*}
	Avec $N_i$ : nombre de particules avec l'énergie $\epsilon_i$ et $N_{tot}$ nombre
	total de particules : $\sum_{k=1}^{L} N_i$ 

	On somme l'équation $( 1 )$ :
	\begin{align*}
		&\sum_{k=1}^{L} \frac{N_i}{N_{tot}}= \sum_{k=1}^{L} C \cdot \exp(-
		\frac{\epsilon_i}{kT}) \\
		\iff& \frac{N_{tot}}{N_{tot}} = 1 = C \cdot \sum_{k=1}^{L}  \exp(-
		\frac{\epsilon_i}{kT})\\
		\iff& C = \frac{1}{\sum_{k=1}^{L} \exp(- \frac{\epsilon_i}{kT})} \numberthis
	\end{align*}

	\subsection{Distribution continue}%
	\label{sub:distribution_continue}
	
	De nombreux systèmes comme les molécules d'un gaz possèdent un spectre continu de
	l'énergie. 

	L'énergie d'une particule est associée à sa position $x$ et à sa vitesse $v_x$. Suite
	à l'agitation thermique, la position et la vitesse ne sont déterminées qu'avec une
	certaine probabilité.

	La probabilité que la position de la particule donnée soit comprise entre $x$ et
	$x+dx$ et que sa vitesse soit comprise entre $v_x$ et $v_x + dv_x$ est proportionnelle
	au facteur de Boltzmann et à l'élément différentiel $dxdv_x$
	
	\begin{align}
		d\Pi( x,v_x )= C \cdot \exp\left(- \frac{\epsilon( x,v_x )}{kT}\right)
	\end{align}
	C'est la distribution continue (en une dimension)

	Cette probabilité est égale au nombre relatif des particules dont la position
	appartient à l'intervalle $\intervalleff{x}{x+dx}$ et dont la vitesse appartient à
	l'intervalle $\intervalleff{v_x}{v_x+dv_x}$. C'est à dire :
	\begin{align*}
		d\Pi ( x, v_x )=\frac{dN( x,v_x )}{N_{tot}}
	\end{align*}
	Avec $dN( x,, v_x )$ le nombre de particules appartenant à cet intervalle.

	On intègre $( 3 )$ :
	\begin{align*}
		&\int_{\text{tous $x$ et $v_x$}} \frac{dN( x,v_x )}{N_tot}=\int_{x_{min}}^{x_{max}} dx
		\int_{v_{x,min}}^{v_{x,max}} \exp(-\frac{\epsilon( x,v_x )}{kT}) dv_x\\
		\implies& 1 = C \int_{x_{min}}^{x_{max}} dx \int_{v_{x,min}}^{v_{x,max}}
		\exp(-\frac{\epsilon( x,v_x )}{kT})dv_x \\
		\implies& C = \frac{1}{\int_{x_{min}}^{x_{max}} dx \int_{v_{x,min}}^{v_{x,max}}
		\exp(-\frac{\epsilon( x,v_x )}{kT})dv_x } \numberthis
	\end{align*}

	Remarque : en 3D, on remplace dans $( 3 )$ $dxdv_x$ par $dxdydzdv_xdv_ydv_z$

	\subsection{Distribution Maxwellienne des vitesses}

	A partir de l'équation $( 3 )$, on démontre l'expression suivante pour le pourcentage
	des molécules d'un gaz parfait dont la norme de vitesse est comprise entre $v$ et
	$v+dv$ 
	
	\begin{align}
		\frac{dN( v )}{N_{tot}} = \left( \frac{m}{2 \pi kT} \right)^{\frac{3}{2}} 4\pi v^2
		\exp\left(-\frac{mv^2}{2kT}\right)dv
	\end{align}
	
	Vitesse la plus probable : $v_p = \sqrt{\frac{2kT}{m}}$
	
	Pour déterminer le pourcentage de molécules ayant $v \in \intervalleff{v_1}{v_2}$, on
	intègre cette fonction entre $v_1$ et $v_2$.

	\section{Théorème d'équipartition de l'énergie}%
	\label{sec:theoreme_d_equipartition_de_l_energie}
	
	Le nombre de degrés de liberté d'une particule est défini comme le nombre de variables
	indépendantes qui déterminent complètement l'énergie de la particule. 

	\begin{figure}[htpb]
		\centering
		\incfig{composantes}
	\end{figure}

	Dans ce cas, on a donc trois degrés de liberté.

	L'énergie cinétique moyenne de l'atome est :
	\begin{align*}
		\epsilon &= \frac{1}{2}mu^2\\
		&= \frac{1}{2} mu_{x}^2+\frac{1}{2}mu_y^2+\frac{1}{2}mu_z^2\\
		&= \epsilon_x+\epsilon_y+\epsilon_z
	\end{align*}

	On a : $U^2=U_x^2+U_y^2+U_z^2$ 

	Et avec l'isotropie des vitesses : $U_x^2=U_y^2=U_z^2=\frac{1}{3}U^2$ 

	Donc on voit que :
	\begin{align*}
		\epsilon_x=\epsilon_y=\epsilon_z=\frac{1}{3}\epsilon =
	\frac{1}{3}\left( \frac{3}{2}kT \right)=\frac{1}{2}kT
	\end{align*}

	L'énergie cinétique d'un atome d'un gaz parfait est distribuée uniformément entre
	trois degrés de liberté. L'énergie moyenne attribuée à un degré de liberté est égale à
	$\frac{1}{2}kT$

	\begin{defi}
		\textbf{Énoncé du théorème de Boltzmann}

		Pour tout système microscopique à la température $T$, l'énergie moyenne attribuée
		à un degré de liberté du système est égale à $\frac{1}{2}kT$
	\end{defi}

	\section{Capacité thermique des gaz parfaits}%
	\label{sec:capacite_thermique_des_gaz_parfaits}
	
	\subsection{Définition}%
	\label{sub:definition}
	
	\begin{defi}
		La capacité thermique molaire est égale à la chaleur qu'il faut fournir à une mole
		de gaz pour augmenter sa température d'un kelvin à volume constant
		\begin{align*}
			C_V = \frac{1}{\nu} \frac{\delta Q_V}{dT} = \frac{1}{\nu} \left(
			\frac{\partial U}{\partial T} \right)
		\end{align*}
	\end{defi}

	\subsection{Gaz monoatomique (He, Ar)}%
	\label{sub:gaz_monoatomique}
	
	\begin{enumerate}
		\item 3 degrés de liberté, la rotation ne change rien

		\item Énergie cinétique moyenne : $\epsilon = \frac{3}{2}kT$

		\item Énergie interne du gaz : 
		\begin{align*}
			U &= N\epsilon \\
			  &= ( \nu N_A )( \frac{3}{2}kT ) \\
			  &= \frac{3}{2}\nu RT
		\end{align*}

		\item Capacité thermique :
		\begin{align*}
			C_V&=\frac{1}{\nu}\left( \frac{\partial U}{\partial T} \right)\\
			   &= \frac{1}{\nu} \frac{\partial}{\partial T}\left( \frac{3}{2}\nu RT \right)\\
			   &= \frac{1}{\nu} \frac{3}{2}\nu R \frac{dT}{dT}\\
			   &= \frac{3}{2}R
		\end{align*}
	\end{enumerate}

	\subsection{Gaz diatomique ( $N_2, O_2$, air )}%
	\label{sub:gaz_diatomique_n_2_o_2_air_}
	
	\begin{enumerate}
		\item En plus des trois degrés de liberté précédents, la molécule peut tourner selon les
		axes $x$, $z$ (on place les deux atomes alignés sur l'axe $y$)


		On a $v_x, v_y, v_z$ pour variables de translations, et $\omega_x, \omega_z$ 
		variables de rotations 
		\item Énergie cinétique moyenne de la molécule :
		\begin{align*}
			\epsilon = 5 \cdot \frac{1}{2} kT
		\end{align*}

		\item Énergie interne du gaz :
			\begin{align*}
				U&= N\epsilon\\
				&=\nu N_A \frac{5}{2}kT\\
				&=\frac{5}{2}\nu RT
			\end{align*}

		\item Capacité thermique
		\begin{align*}
			C_V &= \frac{1}{\nu}\left( \frac{\partial U}{\partial T} \right)_{v=cst}\\
				&= \frac{1}{\nu}\frac{\partial}{\partial T}\left( \frac{5}{2}\nu RT \right) \\
				&= \frac{5}{2}R
		\end{align*}
	\end{enumerate}

	\subsection{Gaz polyatomique}%
	
	\begin{enumerate}
		\item 6 degrés de liberté

		\item Énergie cinétique moyenne de la molécule :
		\begin{align*}
			\epsilon = 3 kT
		\end{align*}

		\item Énergie interne du gaz :
			\begin{align*}
				U&= N\epsilon\\
				&=3\nu RT
			\end{align*}

		\item Capacité thermique
		\begin{align*}
			C_V &= \frac{1}{\nu}\left( \frac{\partial U}{\partial T} \right)_{v=cst}\\
				&= \frac{1}{\nu}\frac{\partial}{\partial T}\left( 3\nu RT \right) \\
				&= 3R
		\end{align*}
	\end{enumerate}

	\subsection{Conclusion}%
	\label{sub:conclusion}
	
	\begin{fml}
		Capacité thermique molaire à $V=cst$ des gaz parfaits :
		\begin{align*}
			C_V = \frac{i}{2}R
		\end{align*}
		Avec $i$ le nombre de degrés de libertés :
		$
			\left\{
			\begin{NiceMatrix}
				3 & \text{monoatomique} \\
				5 & \text{diatomique} \\
				6 & \text{polyatomique}
			\end{NiceMatrix}
			\right.
		$
	\end{fml}
\end{document}
