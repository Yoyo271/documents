\input{preamble}
\usepackage{tikzit}
\input{figures/styles.tikzstyles}

\title{ Moteurs thermiques }

\begin{document}
	\section{Moteurs thermiques}%
	\label{sec:moteurs_thermiques}

	\subsection{Cycle de Carnot : cycle de référence}%
	
	\begin{center}
		\tikzfig{c6-1-1}
		\tikzfig{c6-1-2}
	\end{center}

	Premier principe pour le cycle :
	\begin{align*}
		\underbrace{\Delta U_{cycle}}_{0}&= W_{cycle}+Q_{cycle}=0\\
		\iff& Q_{cycle}=-W_{cycle} \ou \abs{ Q_{cycle} }=\abs{ W_{cycle} } \numberthis
	\end{align*}
	Chaleur du cycle : 
	\begin{equation}
		\abs{ Q_{cycle} }=\underbrace{Q_1}_{>0}-\underbrace{Q_2}_{>0}
	\end{equation}
	Avec $Q_1$ chaleur de source chaude, $Q_2$ chaleur évacuée vers source froide

	Rendement du cycle :
	\begin{align*}
		\eta_{th}&= \frac{\text{effet utile}}{\text{effet couteux}} \\
		&= \frac{\abs{ W_{cycle} }}{Q_1} \\
		&= \frac{Q_1-Q_2}{Q_1} \\
		&= 1- \frac{Q_2}{Q_1} \numberthis
	\end{align*}

	Pour Carnot :
	\begin{align*}
		Q_1&=Aire[A12B]= T_I( S_B-S_A )\\
		Q_2&=Aire[A43B]=T_{II}( S_B-S_A )
	\end{align*}
	On déduit de $( 3 )$ :
	\begin{equation}
		\eta_{carnot} = 1- \frac{T_{II}}{T_I} < 1
	\end{equation}

	\subsection{}%
	
	\begin{itemize}
		\item Schéma : \ctikzfig{c6-1-3}

		$Q_{1, réel}=Aire[E123F]$ <	$Q_{1, carnot}=Aire[EABF]$

		$Q_{2, réel}=Aire[E143F]$ > $Q_{2, carnot}=Aire[EDCF]$

		$\frac{Q_{2, réel}}{Q_{1,réel}} > \frac{Q_{2,carnot}}{Q_{1, carnot}}$

		$1-\frac{Q_{2, réel}}{Q_{1,réel}} < 1-\frac{Q_{2,carnot}}{Q_{1, carnot}}$

		\item Finalement : $\eta_{th, réel} < \eta_{th, carnot}=1-\frac{T_{II}}{T_I} < 1$
	\end{itemize}

	\begin{defi}
		\textbf{Énoncés alternatifs des deux premiers principes} 

		\textbf{1er principe :} aucune machine thermique ne peut produire plus d'énergie
		(sous forme de travail) qu'elle en consomme

		\textbf{2e principe :} aucune machine thermique ne peut produire plus d'énergie
		que la machine de Carnot
	\end{defi}

	\subsection{}%
	
	Schéma : cycle à 4 étapes. Dans le sens des aiguilles d'une montre, en partant du coin
	inférieur gauche : $1234$.

	Rectangle légèrement incliné. Son aire est égale à $\abs{ Q_{cycle} }=\abs{ W_{cycle} }$ 

	Entre $2$ et $3$, $V$ est constant. La combustion a lieu, augmentation de la
	température. C'est $Q_1$

	Entre $4$ et $1$, $V$ est constant. Atmosphère froide, refroidissement. C'est $Q_2$
	
	Chaleur reçue au cours de la combustion à $V$ const :
	\begin{align*}
		Q_1&= \abs{ \nu c_v ( T_3-T_2 ) }=\nu c_v ( T_3-T_2 )
	\end{align*}
	Chaleur cédée à l'atmosphère à $V$ const.
	\begin{align*}
		Q_2&= \abs{ \nu c_v ( T_1-T_4 ) }=\nu c_v ( T_4-T_1 )
	\end{align*}
	Rendement thermique :
	\begin{align*}
		\eta_{th} &= 1-\frac{Q_2}{Q_1}=1-\frac{1}{\epsilon^{\gamma -1}}\\
		\shortintertext{Avec :}
		\epsilon &= \frac{V_1}{V_2} \text{ taux de compression}\\
		\gamma & \text{ exposant adiabatique}
	\end{align*}

	\subsection{}%
	
	Même schéma que le précédent, mais entre 2 et 3, la pression est constante au lieu de
	la température.

	Chaleur reçue au cours de la combustion à $P$ const :
	\begin{align*}
		Q_1&= \abs{ \nu c_p ( T_3-T_2 ) }=\nu c_p ( T_3-T_2 )
	\end{align*}
	Chaleur cédée à l'atmosphère à $P$ const.
	\begin{align*}
		Q_2&= \abs{ \nu c_v ( T_1-T_4 ) }=\nu c_v ( T_4-T_1 )
	\end{align*}
	Rendement thermique :
	\begin{align*}
		\eta_{th} &= 1-\frac{Q_2}{Q_1}=1-\frac{c_v ( T_4-T_1 )}{c_p( T_3-T_2 )}\\
				  &= \frac{T_4-T_1}{\gamma ( T_3-T_2 )}
	\end{align*}

	\section{Enthalpie H(T, P). Entropie $S( T,P )$}%
	
	\setcounter{equation}{0}

	\begin{enumerate}
		\item 
		On choisit des variables indépendantes $T,P$.

		\begin{align*}
			H=f_1( T,P,V( T,P ) )
		\end{align*}
		Différentielle totale dé $H$ :
		\begin{align*}
			dH = \left(  \frac{\partial H}{\partial T} \right)_P dT + \left( \frac{\partial
			H}{\partial P} \right)_T dP \numberthis
		\end{align*}

		\item $\left( \frac{\partial H}{\partial T} \right)_P$

		Rappel : 
		\begin{align*}
			H=U+PV\implies dH = dU + PdV + VdP \numberthis
		\end{align*}
		1er principe pour transfo infinitésimale :
		\begin{align*}
			dU &= \delta W + \delta Q = -PdV+\delta Q \numberthis\\
			( 3 ) \to ( 2 )\implies dH&= \delta Q + VdP \numberthis
		\end{align*}
		A $P=const$ on a $dP=0$ et :
		\begin{align*}
			( 4 ) \implies& dH = \delta Q_p = \nu c_p dT\\
			\implies& \left( \frac{\partial H}{\partial T} \right)_P = \nu c_p \numberthis
		\end{align*}

		\item $\left( \frac{\partial H}{\partial P} \right)_T$ 

		1er principe pour transfo infinitésimale et réversible :
		\begin{align*}
			\delta Q = TdS \et ( 4 )\implies dH = TdS + VdP \numberthis
		\end{align*}
		\begin{align*}
			\left.
			\begin{NiceMatrix}
				\left( \frac{\partial H}{\partial P} \right)_T = T\left( \frac{\partial
				S}{\partial P} \right)_T + V\\
				\text{On prouve que :} \\
				\left( \frac{\partial S}{\partial P} \right)_T = - \left( \frac{\partial
				V}{\partial T} \right)_P
			\end{NiceMatrix}
		\right\} \implies ( \frac{\partial H}{\partial P} )_T = -\left( T\left(
			\frac{\partial	V}{\partial T} \right)_P - V \right) \numberthis
		\end{align*}
		
		\item $( 5 ),( 7 ) \to ( 1 ) \implies$ :
		\begin{fml}
			\begin{align*}
				dH = \nu c_p dT - \left( T\left( \frac{\partial V}{\partial T} \right)_P
				\right) dP \numberthis
			\end{align*}
		\end{fml}
		Cas particulier des gaz parfaits : $dH = \nu c_p dT$

		\item Entropie

		\begin{align*}
			( 6 )\implies dS &= \frac{1}{T} dH - \frac{V}{T}dP = \nu c_p \frac{dT}{T}-(
			\frac{\partial V}{\partial T} )_P dP + \frac{V}{T}dP -\frac{V}{T}dP\\
							 &= \nu c_p \frac{dT}{T}-\left( \frac{\partial V}{\partial T} \right)_P
							 dP \numberthis
		\end{align*}

		A la différence des gaz parfaits, les capacités thermiques molaires $c_v$ et $c_p$ 
		des gaz réels dépendent dé $T,V,P$ 

		\begin{fml}
			\begin{align*}
				\left( \frac{\partial c_v}{\partial V} \right)_T &= \frac{T}{\partial} \left(
				\frac{\partial^2 P}{\partial T^2} \right)_V \to c_v = f( T,V )\\
					\left( \frac{\partial c_p}{\partial P} \right)_T &= -\frac{T}{\partial} (
				\frac{\partial^2V}{\partial T^2} )_P \to  c_p =f( T,P )
			\end{align*}
		\end{fml}
	\end{enumerate}

	\section{Détente de Joule Thomson}%
	\begin{enumerate}
		\item Réalisation

		Réservoir pressurisé contenant un gaz, pression $P_1$. Petite ouverture avec
		diaphragme. Extérieur : $P_2 \ll P_1$. $Q=0$

		Lorsqu'un gaz ou un liquide sort d'un réservoir pressurisé à travers un canal
		isolé thermiquement et muni d'un obstacle, sa pression diminue de $P_1$ en
		amont de l'obstacle jusqu'à $P_2$ en aval de l'obstacle.

		Ainsi le gaz subit une détente appelée "détente de Joule Thomson"
		(adiabatique, irréversible).

		\ctikzfig{c6-3-1}

		Le système est le gaz dans $ABCD$. L'extérieur est le reste du gaz.

		Au temps $t$ : le gaz est dans $ABCD$.
		Au temps $t+dt$ : le gaz est dans $A'B'C'D'$.

		État initial, 		
		\begin{align*}
			U_{ini}&=\underbrace{U_1}_{ABB'A'}+\underbrace{U_0}_{A'B'CD}\\
			U_{fin}&=\underbrace{U_0}_{A'B'CD}+\underbrace{U_2}_{DCC'D'}\\
			\Delta U &= U_2-U_1
		\end{align*}

		Travail reçu :
		\begin{align*}
			W_{ext\to gaz}&= W_{\text{pression gauche}}+W_{\text{pression droite}} \\
						  &= F_1x_1-F_2x_2, x_1=A A', F_1=\text{ force vers AB}\\
						  &= P_1V_1-P_2V_2
		\end{align*}
		1er principe :
		\begin{align*}
			\Delta U &= W_{ext\to sys}+\underbrace{Q_{ext\to sys}}_{0}\\
					 &= P_1V_1-P_2V_2\\
			\implies& U_2+P_2V_2=U_1+P_1V_1\\
			\implies& H_2=H_1
		\end{align*}
		Au cours de la détente de Joule Thomson, l'enthalpie du gaz ou du liquide ne varie
		pas. Cette détente est appelée isenthalpique.

		\item Application aux gaz parfaits
		\begin{align*}
			\Delta H = \nu c_p \Delta T = 0 \implies \Delta T = 0 
		\end{align*}
		La température du gaz parfait ne varie pas au cours de la détente de Joule Thomson

		\item Application aux gaz réels
		\begin{align*}
			dH &= \nu c_p \Delta T - \underbrace{( t( \frac{\partial v}{\partial t}
			)_p}_{\neq 0}-V )dP = 0\\
				dP &< 0, dT \neq 0 \text{ souvent } <0
		\end{align*}
		Application : frigo, température du fluide frigorigène diminue.
	\end{enumerate}

	
\end{document}
