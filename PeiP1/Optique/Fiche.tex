\input{preamble}


\begin{document}
	\begin{defi}
	\textbf{Diffraction} : la diffraction est le comportement des ondes lorsqu’elles
	rencontrent un obstacle ou une ouverture de dimension comparable ou plus petite que la
	longueur d'onde, la lumière est diffusée au travers de celui-ci.
	\end{defi}

	\begin{defi}
			\textbf{Longueur d'onde} : période spatiale d'une oscillation d'une onde
			électromagnétique. Notée $\lambda$.
	\end{defi}

\begin{theo}
		La propagation de la lumière est rectiligne dans un milieu transparent homogène.
\end{theo}

\begin{defi}
		\textbf{Réflexion} : la réflexion se définit comme un changement de direction du
		rayon lumineux quand celui-ci atteint une surface.

		Il existe deux types de réflexion :
		\begin{itemize}
				\item Réflexion diffuse : la lumière est réfléchie de manière plus ou
				moins isotrope, et notamment vers l'œil...
				\item Réflexion spéculaire : lorsque la surface sur laquelle se réfléchit
				la lumière est lisse, c'est à dire dont les aspérités ont une taille
				inférieure à la longueur d’onde du rayonnement incident, la lumière est
				réfléchie selon une seule direction.
		\end{itemize}
\end{defi}

\begin{rmqs}
		\begin{itemize}
				\item Le rayon lumineux est dit incident avant d'avoir rencontré la
				surface réfléchissante, il est dit "réfléchi" après.
				\item La droite orthogonale à la surface réfléchissante au point
				d'incidence est appelée normale (à la surface réfléchissante).
				\item Le plan contenant le rayon incident et la normale à la surface
				réfléchissante au point d'incidence est dit plan d'incidence.
		\end{itemize}
\end{rmqs}

\begin{theo}
		\textbf{Loi de réflexion : }
		\begin{itemize}
				\item le rayon réfléchi est dans le plan d'incidence (défini par le rayon
				incident et la normale au miroir)
				\item les angles incidents et réfléchis sont égaux en valeurs absolues,
				$\theta_1$ et $\theta_2$ vérifient : $\theta_2 = -\theta_1$
		\end{itemize}
\end{theo}

\begin{defi}
		\textbf{Image} :
		\begin{itemize}
				\item Image réelle : on a de l’énergie au point S’. Toute l’énergie est
				concentrée au point S’.  Intéressant pour réaliser une réaction
				photochimique telle que l’impression d’une pellicule photographique.
				\item Image virtuelle : on n’a pas d ’énergie au point S’. Impossible
				d’avoir l’image sur un écran ou d’impressionner une pellicule
				photographique. Exemple le miroir.
				\item Pas d’image nette : dans le cas ou tous les rayons issus de S ne
				passent par un point S’ alors un point donne une multitude de points. On a
				une image floue ou pas d’image nette.
		\end{itemize}
\end{defi}

\begin{prpts}
		\textbf{Bilan sur les miroirs plans} :
		\begin{itemize}
				\item Image virtuelle
				\item Tous les rayons passent par A' et ceci quelque soit A.
				\item A et A' sont symétriques par construction.
				\item Système unique : c'est le seul système pour lequel la propriété 2
				est vraie.
		\end{itemize}
\end{prpts}

\begin{defi}
		\textbf{Réfraction} :
		Changement de direction de propagation d'une onde électromagnétique en passant
		d'un milieu dans un autre.
\end{defi}

\begin{prpts}
		\begin{itemize}
		\item Lorsqu'elle pénètre dans un milieu d'indice plus élevé, la lumière se
		rapproche de la normale. La réciproque est vraie. Mnémo : \textit{FOTOFAÉ} (\Mag
		F\Mag ort \Mag t\Mag o \Mag f\Mag aible \Mag éloigne)
				\item Loi de Snell Descartes : $n_1 \sin((i_1) = n_2 \sin(i_2)$
		\end{itemize}
\end{prpts}

\begin{rmq}
		Il existe un angle limite au delà duquel il n'existe plus de rayon réfracté. Il y
		a alors réflexion totale. On a $n_1 \sin(1_{1\text{lim}}) = n_2
		\sin(\frac{\pi}{2})$
\end{rmq}

\begin{fmls}
		\textbf{Relation de conjugaison}

		\begin{itemize}
		\item Dioptre plan :
				\[
				\overline{OA'} = \overline{OA} \frac{n_1}{n_2}
				\]
		\item Miroir concave / convexe :
				\[
		\frac{1}{\overline{SA}} + \frac{1}{\overline{SA'}}
		= \frac{2}{\overline{SC}} \ou \overline{SA'} =
		\frac{\overline{SC}\cdot
		\overline{SA}}{2\overline{SA}-\overline{SC}}
				.\]
		\item Dioptre sphérique :
				\[
		\frac{n'}{\overline{SA'}} -
		\frac{n}{\overline{SA}} =
		\frac{n'-n}{\overline{SC}} \ou
		\overline{SA'} =
		\frac{\overline{SC}\times \overline{SA}\times n'}{(n'-n)
		\overline{SA}+n \times  \overline{SC}}
				.\]
		\item Lentille mince :
				\[
				\frac{1}{\overline{OA'}} - \frac{1}{\overline{OA}} =
				\frac{1}{\overline{OF'}} \ou \overline{OA'} = \frac{f' \overline{OA}}{f' +
				\overline{OA}}
				.\]
		\end{itemize}

\end{fmls}

\begin{fmls}
		\textbf{Grandissement}
		\begin{itemize}
				\item Dioptre plan :
						\[
						\frac{n_1}{n_2}
						.\]
				\item Miroir concave / convexe :
						\[
						\gamma = - \frac{\overline{SA'}}{\overline{SA}} = -
						\frac{\overline{SC}}{2\overline{SA}-\overline{SC}}
						.\]
				\item Dioptre sphérique
						\[
							\gamma = \frac{n \overline{SA'}}{n' \overline{SA}}
						.\]
				\item Lentille mince :
						\[
							\gamma = \frac{f'}{f' + \overline{OA}}
						.\]
		\end{itemize}
\end{fmls}

\begin{rmqs}
		Le grandissement d'un miroir sphérique concave est $\le $ 1.

		Le grandissement d'un miroir sphérique convexe est $\ge $ 1.
\end{rmqs}

\begin{fmls}
		\textbf{Grossissement}
		\[
		G = \frac{\alpha'}{\alpha}
		.\] avec $\alpha$ l'angle sous lequel on voit l'objet à l'œil nu, et $\alpha'$
		l'angle sous lequel on voit l'objet à travers l'instrument.

		Le grossissement est utilisé pour caractériser des images lointaines.
\end{fmls}

\begin{fmls}
	\textbf{Vergence d'un dioptre sphérique}

	\[
	V = \frac{n' - n}{\overline{SC}} = \frac{n'}{f'} = -\frac{n}{f}
	.\]
\end{fmls}

\begin{rmqs}
	Si V > 0, le dioptre est convergent. Sinon, il est divergent.
\end{rmqs}

\begin{prpts}
		\textbf{Construction géométrique} :
		\begin{itemize}
				\item Tout rayon passant par le centre du miroir n'est pas dévié.
				\item Tout rayon parallèle incident à l'axe optique passe par le foyer
				image F'.
				\item Tout rayon passant par le foyer objet F émerge parallèlement à l'axe
				optique.
				\item Tout rayon lumineux incident au sommet du miroir émerge avec le même
				angle \textbf{(Miroir concave / convexe uniquement)}.
		\end{itemize}
\end{prpts}

\begin{defi}
	\textbf{Dioptre sphérique} :
	Ensemble constitué de deux milieux transparents, homogènes et isotropes, d'indices
	différents séparés par une surface sphérique. L'axe principal est l'axe
	perpendiculaire au plan de base, il coupe le dioptre en son sommet S. Le milieu
	d'indice n sera qualifié de milieu objet tandis que celui d'indice n' sera qualifié de
	milieu image.
\end{defi}

\begin{defi}
		\textbf{Lentille} :
		Système centré formé de deux dioptres dont au moins l'un est un dioptre sphérique.
\end{defi}

\begin{prpts}
		\begin{itemize}
				\item Pour une lentille mince, le foyer objet et le foyer image sont
				symétriques par rapport à leur sommet O.
				\item $\overline{OF} < 0$ implique que la lentille est convergente. Dans
				le cas contraire, elle est divergente.
		\end{itemize}
\end{prpts}



\end{document}
